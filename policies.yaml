apiVersion: kyverno.io/v1
kind: ClusterPolicy
metadata:
  annotations:
    policies.kyverno.io/category: Pod Security Standards (Restricted)
    policies.kyverno.io/description: Privilege escalation, such as via set-user-ID
      or set-group-ID file mode, should not be allowed. Another change test.
  name: deny-privilege-escalation
spec:
  background: true
  rules:
  - match:
      resources:
        kinds:
        - Pod
    name: deny-privilege-escalation
    validate:
      message: Privilege escalation is disallowed. The fields spec.containers[*].securityContext.allowPrivilegeEscalation,
        and spec.initContainers[*].securityContext.allowPrivilegeEscalation must be
        undefined or set to `false`.
      pattern:
        spec:
          =(initContainers):
          - =(securityContext):
              =(allowPrivilegeEscalation): "false"
          containers:
          - =(securityContext):
              =(allowPrivilegeEscalation): "false"
---
apiVersion: kyverno.io/v1
kind: ClusterPolicy
metadata:
  annotations:
    policies.kyverno.io/category: Pod Security Standards (Default)
    policies.kyverno.io/description: Adding additional capabilities beyond the default
      set must not be allowed. Test changes.
  name: disallow-adding-capabilities
spec:
  background: true
  rules:
  - match:
      resources:
        kinds:
        - Pod
    name: capabilities
    validate:
      message: Adding of additional capabilities beyond the default set is not allowed.
        The fields spec.containers[*].securityContext.capabilities.add and  spec.initContainers[*].securityContext.capabilities.add
        must be empty.
      pattern:
        spec:
          =(initContainers):
          - =(securityContext):
              =(capabilities):
                X(add): null
          containers:
          - =(securityContext):
              =(capabilities):
                X(add): null
---
apiVersion: kyverno.io/v1
kind: ClusterPolicy
metadata:
  annotations:
    policies.kyverno.io/category: Pod Security Standards (Default)
    policies.kyverno.io/description: Host namespaces (Process ID namespace, Inter-Process
      Communication namespace, and network namespace) allow access to shared information
      and can be used to elevate privileges. Pods should not be allowed access to
      host namespaces.
  name: disallow-host-namespaces
spec:
  background: true
  rules:
  - match:
      resources:
        kinds:
        - Pod
    name: host-namespaces
    validate:
      message: Sharing the host namespaces is disallowed. The fields spec.hostNetwork,
        spec.hostIPC, and spec.hostPID must not be set to true.
      pattern:
        spec:
          =(hostIPC): "false"
          =(hostNetwork): "false"
          =(hostPID): "false"